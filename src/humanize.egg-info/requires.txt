
[:python_version < "3.8"]
importlib-metadata

[tests]
freezegun
pytest
pytest-cov
